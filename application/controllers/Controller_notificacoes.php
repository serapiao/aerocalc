<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_notificacoes extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_notificacoes');
		    
	}


	public function criar_notificacao(){

		$this->form_validation->set_rules('titulo_notificacao','Título','required');
		$this->form_validation->set_rules('notificacao','Descrição','required');

		$dados = array (

			'fk_usuario'              => $this->session->userdata('usuario'),
			'titulo_notificacao'      => $this->input->post('titulo_notificacao'),
			'notificacao'             => $this->input->post('notificacao'),
			'data_limite_notificacao' => $this->data($this->input->post('data_limite_notificacao'))

		);

		if ($this->form_validation->run()) {

			$this->model_notificacoes->start();
			$id = $this->model_notificacoes->create($dados);

			$this->model_notificacoes->notificar($id,$this->input->post('fk_usuario_destino'),$this->input->post('id_grupo'),$this->input->post('fk_usuario_nao_destino'));

			$commit = $this->model_notificacoes->commit();
			
			if ($commit['status']) {
				$this->aviso('Notificação enviada','Notificação enviada com sucesso!','success',false);

				redirect('main/redirecionar/11/'.$id);
			} else {

				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/10');
			}

		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/10');

		}

	}

	public function editar_notificacao(){

		$this->form_validation->set_rules('titulo_notificacao','Título','required');
		$this->form_validation->set_rules('notificacao','Descrição','required');

		$dados = array (

			'id_notificacao'          => $this->input->post('id_notificacao'),
			'fk_usuario'              => $this->session->userdata('usuario'),
			'titulo_notificacao'      => $this->input->post('titulo_notificacao'),
			'notificacao'             => $this->input->post('notificacao'),
			'data_limite_notificacao' => $this->data($this->input->post('data_limite_notificacao'))

		);

		if ($this->form_validation->run()) {

			$this->model_notificacoes->start();
			$this->model_notificacoes->update($dados);

			$commit = $this->model_notificacoes->commit();
			
			if ($commit['status']) {
				$this->aviso('Notificação editada','Notificação editada com sucesso!','success',false);

				redirect('main/redirecionar/12/'.$this->input->post('id_notificacao'));
			} else {

				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/11/'.$this->input->post('id_notificacao'));
			}

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/11/'.$this->input->post('id_notificacao'));

		}

	}

	public function load_notificacao(){

		$usuario = $this->session->userdata('usuario');
		$id = $this->input->post('id');

		$this->model_notificacoes->start();
		$notificacao = $this->model_notificacoes->loadNotificacao($usuario,$id);

		$commit = $this->model_notificacoes->commit();
			
		if ($commit['status']) {

			echo "<strong>Título: </strong> {$notificacao->titulo_notificacao} <br>
				  <strong>Descrição: </strong> <br> 
				  	{$notificacao->notificacao}
				  <br>
				  <hr>";

			echo "<small>Data Publicação: {$notificacao->data_envio_notificacao}</small> / ";
			
			if ($notificacao->data_limite_notificacao != 0)
				echo "<small>Disponível até: {$notificacao->data_limite_notificacao}</small>  / ";
			
			if ($notificacao->data_leitura != 0)
				echo "<small>Lido em: {$notificacao->data_leitura}</small> ";

		} else {
			echo 'Erro ao Carregar: '.$commit['message'].'';
		}


	}

	public function ajax_Notificacoes(){
		
		header('Content-Type: application/json; charset=utf-8');

		$this->model_notificacoes->start();
		
		$dados = $this->model_notificacoes->ajaxNotificacoes();

		$commit = $this->model_notificacoes->commit();
			
		if ($commit['status'])
			echo json_encode($dados);	

	}

	public function upload_imagem(){

		$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/notificacoes/';

		//Gera um nome único para o arquivo
		$arquivo = basename(time().uniqid(md5($_FILES['imagem']['name']))).'.png';
		$uploadfile = $uploaddir.$arquivo;
		$editor = $_POST['editor'];

		echo '<pre>';
		if (move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)) {
		    echo "Arquivo válido.\n";
		} else {
		    echo "Arquivo inválido!\n";
		}

		echo 'DEBUG:';
		print_r($_FILES);

		echo '</pre>';

		$image_type = array('image/jpeg', 'image/jpg', 'image/gif', 'image/png', 'image/x-png', 'image/bmp');

		echo '<script>';
		if(in_array($_FILES['imagem']['type'], $image_type)) {
			echo 'window.parent.'.$editor.'.insertContent("<img src=\''.base_url().'upload/notificacoes\/'.$arquivo.'\' />");';
		} else {
			echo 'window.parent.'.$editor.'.insertContent("Erro: Arquivo num formato inválido");';
			unlink($uploadfile);
		}
		echo 'window.parent.'.$editor.'.plugins.upload.finish();';
		echo '</script>';
		
	}	

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

	public function data($data = null){
		
		if ($data != "" && $data != null) {
			$data_ = explode('/',$data);
			return $data_[2].'-'.$data_[1].'-'.$data_[0];
		} else {
			return 0;
		}

	}

}