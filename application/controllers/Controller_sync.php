<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_sync extends CI_Controller {

	function __construct() {
			// Construct the parent class
			parent::__construct();
			$this->load->model('model_sync');
		}


	public function sync_aeroportos_get() {

			header('Content-Type: text/json; charset=utf-8');

			if ($this->autenticacao()) {
			$retornar = $this->model_sync->getSyncAeroporto();
			$this->response($retornar,Self::HTTP_OK); //200
			}
		}

		public function sync() {

			$last_id = $this->input->get('last_id');

			header('Content-Type: text/json; charset=utf-8');

			$sincroniza = $this->model_sync->getSync($last_id);

			if (!$sincroniza) {
				echo json_encode(array('status' => 'Atualizado'));
			} else {
				foreach ($sincroniza as $key => $value) {
					$dados = array(
						'id' => $value['id_linha'],
						'tabela' => $value['tabela'],
						'pk' => $this->model_sync->getColunaId($value['tabela'])
					);

					$linha[] = $this->model_sync->getSyncLinha($dados);
				}

				//print_r($linha);
				echo json_encode($linha);
			}
		}
	}