<?php defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . '/libraries/REST_Controller.php';
	use Restserver\Libraries\REST_Controller;

	class Controller_webservice extends REST_Controller  {

		function __construct() {
			// Construct the parent class
			parent::__construct();
			// Configure limits on our controller methods
			// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
			$this->methods['perfil_get']['limit'] = 500; // 500 requests per hour per user/key
			$this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
			$this->methods['perfileditar_put']['limit'] = 50; // 50 requests per hour per user/key
			$this->load->model('model_webservice');
			$this->load->model('model_usuarios');
		}

		public function autenticacao(){ //Garante que quem está usando o sistema possúi credenciais de acesso.

			if (!isset($_SERVER['PHP_AUTH_USER']) 
				|| empty($_SERVER['PHP_AUTH_USER'])
				|| !isset($_SERVER['PHP_AUTH_PW']) 
				|| empty($_SERVER['PHP_AUTH_PW'])) { //Usuário e / ou senha em Branco
			    
				return true;

			} else {

			    $this->model_webservice->set_('usuario',$_SERVER['PHP_AUTH_USER']);
				$this->model_webservice->set_('senha',sha1($_SERVER['PHP_AUTH_PW']));
				if ($this->model_webservice->validar_login()) {
					
					return true;

				} else {

					return true;

				}

			}

		}

		//http://localhost/prototipo/controller_webservice/perfil_login/senha/123/usuario/admin
		public function perfil_login_get() {

			header('Content-Type: text/html; charset=utf-8');

			if ($this->autenticacao()) {
				
				$valores = array('usuario' => $this->get('usuario'), 'senha' => $this->get('senha'));

				$this->form_validation->set_data($valores);
				$this->form_validation->set_rules('usuario','Nome do usuário','required');
				$this->form_validation->set_rules('senha','Senha','required');

				if ($this->form_validation->run()) {

					$this->model_webservice->set_('usuario',$valores['usuario']);
					$this->model_webservice->set_('senha',sha1($valores['senha']));
					$dados = $this->model_webservice->validar_login();

					if ($dados) {

						$retornar = array(

							'erro_acesso' => "", //Adiciona um campo
							'id_usuario' => $dados->id_usuario,
							'nome_usuario' => $dados->nome_usuario,
							'email_usuario' => $dados->email_usuario,
							'login_usuario' => $dados->login_usuario,
							'fk_grupo_usuario' => $dados->fk_grupo_usuario

						);
						
						//DEFINIR REGRAS DE GRUPOS **************************************
						if ($dados->fk_grupo_usuario == 0) {
							
							$this->response(array('erro_acesso' => 'Usuário sem permissão de acesso.'),Self::HTTP_UNAUTHORIZED); //401

						} else if($dados->ativo_usuario == 0) {

							$this->response(array('erro_acesso' => 'Usuário inativo.'),Self::HTTP_UNAUTHORIZED); //401

						} else {

							$this->response($retornar,Self::HTTP_OK); //200
						

						}

					} else {
						$this->response(array('erro_acesso' => 'Dados não localizados!'),Self::HTTP_NOT_FOUND); //404
					}

				} else { //Campos Preenchidos

					$erros = validation_errors();
					$this->response(array('erro_acesso' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

				}

			} else {

				$this->response(array('erro_acesso' => 'Usuário e/ou Senha não definidos ou incorretos.'),Self::HTTP_UNAUTHORIZED); //401

			}
		
		}

		//http://localhost/prototipo/controller_webservice/criar_usuario/nome_usuario/teste/email_usuario/aabcom/telefone_usuario/1112312312/login_usuario/teste/senha_usuario/aaaaaa/ativo_usuario/1/fk_grupo_usuario/1
		public function criar_usuario_post(){

			header('Content-Type: text/html; charset=utf-8');

			if ($this->autenticacao()) {

				$valores = array(
						'nome_usuario' => $this->get('nome_usuario'), 
						'email_usuario' => $this->get('email_usuario'),
						'telefone_usuario' => $this->get('telefone_usuario'),
						'login_usuario' => $this->get('login_usuario'),
						'senha_usuario' => $this->get('senha_usuario'),
						'ativo_usuario' => $this->get('ativo_usuario'),
						'fk_grupo_usuario' => $this->get('fk_grupo_usuario')
					);

				$this->form_validation->set_data($valores);

				$this->form_validation->set_rules('nome_usuario','Nome do Usuário','required');
				$this->form_validation->set_rules('email_usuario','E-mail do Usuário','required|is_unique[seg_usuarios.email_usuario]');
				$this->form_validation->set_rules('telefone_usuario','Telefone do Usuário','required');
				$this->form_validation->set_rules('login_usuario','Login do Usuário','required|is_unique[seg_usuarios.login_usuario]');
				$this->form_validation->set_rules('senha_usuario','Senha do Usuário','required');
				$this->form_validation->set_rules('ativo_usuario','Status do Usuário','required');
				$this->form_validation->set_rules('fk_grupo_usuario','Grupo do Usuário','required');

	  			if ($this->form_validation->run()) {

					$dados = $this->model_usuarios->create($valores);

					if ($dados) {
						$this->response(array('id_usuario' => $dados),Self::HTTP_CREATED); //201
					} else {
						$this->response('NOK: Dados não localizados!',Self::HTTP_NOT_FOUND); //404
					}

				} else { //Campos Preenchidos

					$erros = $this->post('nome_usuario').validation_errors();

					$this->response('NOK: '.str_replace("\r\n","",$erros),Self::HTTP_BAD_REQUEST); //400

				}  
	
			} else {

				$this->response('Usuário e/ou Senha não definidos ou incorretos.',Self::HTTP_UNAUTHORIZED); //401

			}

		}

		public function deletar_perfil_delete(){

			$this->response(array('id_usuario' => $this->delete('id_usuario')),Self::HTTP_OK); //201

		}

		public function esqueci_senha_get(){

			header('Content-Type: text/html; charset=utf-8');

			$valores = array('login_usuario' => $this->get('login_usuario'));

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('login_usuario','Login do Usuário','required');

  			if ($this->form_validation->run()) {

				$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
				$caractereVetor = explode(',',$caracteres);
				$senha = '';

				while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
					
					$indice = mt_rand(0, count($caractereVetor) - 1);
					$senha .= $caractereVetor[$indice];

				}

				$email = $this->model_usuarios->senha_Email(sha1($senha),$this->get('login_usuario'));

				//Usuário inativo ou desativado
				if($email == ""){

					$this->response('Dados incorretos ou perfil inativo!',Self::HTTP_NOT_FOUND); //404

				} else { //Senha enviada para o E-mail.

					// Detalhes do Email. 
					$this->email->from('megamil@megamil.net', 'MEGAMIL | Troca de senha'); 
					$this->email->to($email); 
					$this->email->subject('MEGAMIL | Troca de senha'); 
					$this->email->message('<h1>
											<a href="http://'.$_SERVER['HTTP_HOST'].base_url().'">
												MEGAMIL
											</a>
										   </h1> 
									Recebemos sua solicitação de nova senha. <br> 
									Sua nova senha agora é: <strong>'.$senha.'</strong><br>
									<hr>
									<small>Solicitado pelo IP: '.$_SERVER['REMOTE_ADDR'].'</small> <br>
									<small>Sistema / Navegador: '.$_SERVER['HTTP_USER_AGENT'].'</small> <br>
									Em: '.date('d/m/Y H:i:s').'<br>
									<img src="http://'.$_SERVER['HTTP_HOST'].base_url().'/style/img/rodape.png" alt="Rodapé">');   

					// Enviar... 
					if ($this->email->send()) { 

						$this->response(array('resultado' => "Nova Senha enviada para o E-mail: (".$email.")"),Self::HTTP_OK); //200

					} else {

						$this->response("Erro ao enviar senha: ".$this->email->print_debugger(),Self::HTTP_BAD_REQUEST); //400

					}

				}


			} else { //Campos Preenchidos

				$erros = $this->post('login_usuario').validation_errors();

				$this->response('NOK: '.str_replace("\r\n","",$erros),Self::HTTP_BAD_REQUEST); //400

			}  

		}

	}