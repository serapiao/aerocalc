<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_grupos extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		function __construct() {
		    parent::__construct();
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### Querys ###############################

		// Lista dos grupos
		public function view_grupos(){

			/*[ALTERAR CONFORME PRECISE]*/
			$campos_select = "'id','nome_grupo','descricao_grupo','usuarios_ativos','ativo'";
			//As aspas simples não são aceitas como campo, mas para lógica de informar quais campos estão sendo passados "Váriavel campos no array" é preciso.

			$this->db->select(str_replace("'", "", $campos_select));
			$this->db->limit(100);

			/*[ALTERAR CONFORME PRECISE]*/
			return array(

						 'resultado' => 
							$this->db->get("view_lista_grupos")->result_array(),
						 'usuarios' => 
						 	$this->db->query('SELECT id_usuario, nome_usuario from seg_usuarios')->result(),
						 'campos' => /*Carrega as informações para montar a lista automaticamente 
						 				(Caso não tenha uma view listar os campos manualmente)*/
						 	$this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = 'view_lista_grupos' and visivel = true;")->result()
						);
		}

		public function view_editar_grupo($where = null){
			
			$grupo = $this->db->get_where('seg_grupos', array('id_grupo' => $where[0]))->row();

			if (isset($grupo)) {
				foreach ($grupo as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			return array('aplicacoes' => $this->db->query("SELECT 
															id_controller,
															descricao_controller,
															id_aplicacao,
															titulo_aplicacao,
															descricao_aplicacao,
																(select (count(*) = 1) from seg_aplicacoes_grupos where fk_grupo = {$where[0]} and fk_aplicacao = id_aplicacao) permissao
															from seg_controllers
															inner join seg_aplicacao on fk_controller = id_controller
															where id_controller > 1 order by id_controller;")->result());

		}

		public function aplicacoes_grupos($grupo = null, $aplicacoes = null){

			$this->db->where(array('fk_grupo' => $grupo));
			$this->db->delete('seg_aplicacoes_grupos');

			if (count($aplicacoes) > 0) {
				foreach ($aplicacoes as $aplicacao) {

					$valores['fk_grupo'] = $grupo;
					$valores['fk_aplicacao'] = $aplicacao;
					$this->db->insert('seg_aplicacoes_grupos',$valores);

				}
			}
			

			return true;

		}

		public function update($valores = null){

			//Alterar
			$tabela = "seg_grupos";
			$id = 'id_grupo';
			
			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				}
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_grupos / update';
				return false;		
			} else {
				return true;
			}

		}

		public function create($valores = null){

			$this->db->insert('seg_grupos',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_grupos / create';
				return 100;		
			} else {
				return $this->db->insert_id();
			}

		}


	}