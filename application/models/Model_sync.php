<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_sync extends CI_Model {

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			} else {
			    $this->db->trans_commit();
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}

		############################### Querys ###############################

		public function getSyncAeroporto() {
			return $this->db->get('cad_aeroporto')->result();
		}

		public function getSyncLinha($valores = null) {

			$this->db->select("'{$valores['tabela']}' tabela, '{$valores['pk']}' pk, {$valores['tabela']}.*");

			return $this->db->get_where($valores['tabela'], array($valores['pk'] => $valores['id']))->result_array();
		}

		public function getColunaId($tabela = null) {

			return $this->db->query("select column_name from information_schema.COLUMNS where column_key = 'PRI' and table_name = '{$tabela}'")->row()->column_name;
		}

		public function getSync($id = null) {
			
			$result = $this->db->query("select tabela, id_linha from sync, sync_tabela where fk_sync_tabela = id_sync_tabela and id_sync > {$id}");

			if ($result->num_rows() == 0) {
				return FALSE;
			} else {
				return $result->result_array();
			}
		}
	}