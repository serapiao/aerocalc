<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends CI_Model {

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			} else {
			    $this->db->trans_commit();
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}

		############################### Querys ###############################

		private $usuario;
		private $senha;

		public function validar_login(){

			$this->db->select('*');
			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('login_usuario',$this->get_('usuario'));
			$this->db->where('senha_usuario',$this->get_('senha'));
			$login = $this->db->get()->row();

			if (isset($login) && $login->ativo_usuario) {
				return $login;
			} else {
				return false;
			}

		}


		public function set_($campo,$valor){
			$this->$campo = $valor;
		}

		public function get_($campo){
			return $this->$campo;
		}
	}