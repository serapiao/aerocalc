<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
  <meta http-equiv="refresh" content="600">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php 

    if (isset($titulo_aplicacao)){ 

        echo '<title>Protótipo | '.$titulo_aplicacao.'</title>';

     } else { 
	 
        echo '<title>Protótipo</title>';

     }

  ?>
  <link rel="shortcut icon" href="<?php echo base_url() ?>style/img/favicon.ico">
	<link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/select2.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/estilo.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/plugin_tinymce.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/daterangepicker.css">


	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
  <!-- Aceita Mascaras nas listas -->
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/sweetalert.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/select2.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.priceformat.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/tinymce/tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/plugin_tinymce.js"></script>
  
  
  <!-- DataTable -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script> 
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>

  <script type="text/javascript" DEFER="DEFER">
    /*Só é executado após o carregamento total da página*/
    $(document).ready(function(){

      $( "#load_geral" ).fadeOut( "slow", function() {
        $('.principal').show();
      });

      /*Usar notificações no navegador para informações remotas e a qualquer momento, com o navegador aberto*/
      setInterval(function () {
        console.log("Buscando notificações...");
        
        $.ajax({url: '<?php echo base_url(); ?>Controller_notificacoes/ajax_Notificacoes',
                type: 'json',
                method: 'POST'
        }).done(function(dados){

          $.each(dados,function(key,value){

            var usuario = dados[key].nome_usuario;
            var titulo = dados[key].titulo_notificacao;

            if (Notification.permission=='granted') {

                notificar = new Notification('Nova Notificação de: '+usuario,{
                  'body' : titulo,
                  'icon' : '<?php echo base_url() ?>style/img/favicon.ico',
                  'tag' : '1'
                });

                setTimeout(notificar.close.bind(notificar), 5000);
                console.log(Notification.permission);

              } else {

                $.toast({
                      icon: 'info',
                      position: 'top-right',
                      hideAfter: false,
                      heading: 'Nova Notificação de: '+usuario,
                      text: titulo
                  });

                console.log('Por favor, Habilite as Notificações '+Notification.permission);
                console.log(Notification.permission);
              }

          });

        })

      }, 60000);
      

      <?php if ($this->session->flashdata('tipo_alerta') != "") {
        echo "$.toast({
                icon: '".$this->session->flashdata('tipo_alerta')."',
                heading: '".$this->session->flashdata('titulo_alerta')."',
                text: '".$this->session->flashdata('mensagem_alerta')."',";
                if ($this->session->flashdata('mensagem_fixa')) {
                  echo "hideAfter: false,";
                } else {
                  echo "showHideTransition: 'fade',";
                }
                echo "position: 'top-right',
            });";

        $this->session->set_flashdata('tipo_alerta','');
        $this->session->set_flashdata('titulo_alerta','');
        $this->session->set_flashdata('mensagem_alerta','');
        $this->session->set_flashdata('mensagem_fixa','');

      }

    ?>

    $('#erro_feedback').click(function(){

      var cod = $(this).attr('cod');

      swal({
          title: "Nos Ajude",
          text: "Por favor, tente descrever o que estava fazendo para ajudar a corrigir este erro.",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          animation: "slide-from-top",
          inputPlaceholder: "Descreva o que estava fazendo."
        },
        function(inputValue){
          
          if (inputValue != "") {

            $.ajax({
              type: "post",
              url: "<?php echo base_url(); ?>Main/reportar_Erro",
              data:{id_log_erro: cod, 
                    erro_feedback: inputValue},
              error: function(returnval) {
                 mensagem('Error',returnval,'error');
              },
              success: function (returnval) {
                if(returnval == 'sucesso') {

                  swal("Obrigado!", "Obrigado pelo Feedback!");


                } else {

                  swal("Falha =/", "Falha ao reportar erro! "+returnval);

                }

              }
          })

          }
          
        });
      });


    });
  </script>

  <!-- Usado quando o usuário desabilitou o JavaScript -->
  <noscript>
    <meta http-equiv="refresh" content=1;url="http://enable-javascript.com/pt/">
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
      <h1 style="color: white;">HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.</h1>
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

    </div>
  </noscript> 

</head>
<body>

<?php if (isset($menu)) { ?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>style/img/favicon.ico" width="35px" height="30px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php echo $menu; ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php } ?>

<!--Load para troca entre views-->
<div class="row" id="load_geral" style="margin-top: 10%;s">
  
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <img src="<?php echo base_url() ?>style/img/loading.gif" id="img_load_geral" style="width: 400px;">
  </div>

</div>

<!--Div Principal-->
<div class="container principal" hidden="hidden">

<div class="modal fade" id="modal_historico" tabindex="-1" role="dialog" aria-labelledby="modal_historicoLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_historicoLabel">Histórico de edições</h4>
      </div>
      <div class="modal-body">

        <div class="progress_modal_historico">
      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
        <span class="sr-only">50%</span>
      </div>
    </div>

        <div id="load_modal_historico"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    $('.progress_modal_historico').hide();

    $('#historico_modal').click(function () {
        
        $('.progress_modal_historico').show();
        var id = $(this).attr('cod');

        $('#load_modal_historico').load('<?php echo base_url() ?>Controller_relatorios/load_historico_edicoes',{id: id},function(){
          $('.progress_modal_historico').hide();

          dataTableLoad();

        });

    });

  });
</script>
