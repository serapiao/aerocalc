<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Notificações Enviadas</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/10">
			<i class="glyphicon glyphicon-plus-sign"></i> Nova Notificação
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th style="width: 60px;"></th>
		<th>Título</th>
		<th>Enviada Em:</th>
		<th>Enviados / Lidos</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $notificacao) {
			echo '<tr>';
			echo '<td style="width: 60px;"><a href="'.base_url().'main/redirecionar/11/'.$notificacao->id_notificacao.'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
			echo '<td>'.$notificacao->titulo_notificacao.'</td>';
			echo '<td>'.$notificacao->data_notificacao.'</td>';

			$notificados = $notificacao->notificados;
			$lidos = $notificacao->lidos;

			echo '<td>'.$notificados.'/'.$lidos.' ('.percentual($notificados,$lidos).'% Já leram)</td>';
			echo '</tr>';
		}

		function percentual($notificados,$lidos){

			if ($notificados > 0 && $lidos > 0) {
				return (($lidos / $notificados) * 100);
			} else {
				return 0;
			}

		}

	?>
	</tbody>
</table>