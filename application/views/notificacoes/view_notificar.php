<script type="text/javascript">
	history.replaceState({pagina: "lista_notificacoes"}, "Lista de notificações", "<?php echo base_url() ?>main/redirecionar/10");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Nova Notificação</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_notificacoes/criar_notificacao'); ?>

<div class="row">
	
	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_usuario_destino">Enviar Para o(s) Usuário(s)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control" id="fk_usuario_destino" name="fk_usuario_destino[]" aviso="Enviar Para O(s) Usuário(s)" multiple="multiple" style="width: 100%;">
			<option value="">Selecione...</option>
			<?php 

				foreach ($dados_iniciais['usuarios'] as $usuario) {
					echo '<option value="'.$usuario->id_usuario.'">'.$usuario->nome_usuario.'</option>';
				}

			 ?>
			</select>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="id_grupo">Enviar Para Todos do(s) Grupo(s)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control" id="id_grupo" name="id_grupo[]" aviso="Enviar Para Todos Do(s) Grupo(s)" multiple="multiple" style="width: 100%;">
			<option value="">Selecione...</option>
			<?php 

				foreach ($dados_iniciais['grupos'] as $grupo) {
					echo '<option value="'.$grupo->id_grupo.'">'.$grupo->nome_grupo.'</option>';
				}

			 ?>
			</select>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_usuario_nao_destino">NÃO Enviar Para o(s) Usuário(s)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control" id="fk_usuario_nao_destino" name="fk_usuario_nao_destino[]" aviso="Enviar Para O(s) Usuário(s)" multiple="multiple" style="width: 100%;">
			<option value="">Selecione...</option>
			<?php 

				foreach ($dados_iniciais['usuarios'] as $usuario) {
					echo '<option value="'.$usuario->id_usuario.'">'.$usuario->nome_usuario.'</option>';
				}

			 ?>
			</select>
		</div>
	</div>


</div>

<div class="row">

	<div class="col-md-10">
		<div class="form-group has-feedback">
			<label class="control-label" for="titulo_notificacao">Título Notificação</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="titulo_notificacao" name="titulo_notificacao" placeholder="Título Notificação" aviso="Título Notificação" value="<?php echo $this->session->flashdata('titulo_notificacao'); ?>" maxlength="20">
		</div>
	</div>

	<div class="col-md-2" title="Caso a Notificação só possa ser lida por um certo tempo.">
		<div class="form-group has-feedback">
			<label class="control-label" for="data_limite_notificacao">Data limite (Opcional)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_data" id="data_limite_notificacao" name="data_limite_notificacao" placeholder="Descrição do Grupo" aviso="Descrição do Grupo" value="<?php echo $this->session->flashdata('data_limite_notificacao'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group has-feedback">
			<label class="control-label" for="notificacao">Notificação</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<textarea class="form-control" id="notificacao" name="notificacao" placeholder="Notificação" aviso="Notificação"><?php echo $this->session->flashdata('notificacao'); ?></textarea>
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-send"></i> Criar </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		tinymce.init({
			selector: 'textarea',
			language: 'pt_BR',
			height: 250,
			theme: 'modern',
			upload_action: '<?php echo base_url(); ?>Controller_notificacoes/upload_imagem',
			upload_file_name: 'imagem',
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars  fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality upload',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc '

			],
			toolbar1: 'undo redo |  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image upload',
			// toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | styleselect ',
			image_advtab: true,
			templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
			],
			content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
			]
		});

		$('#id_grupo').select2({
			tags: false,
			tokenSeparators: [';', ' ']
		});

		$('#fk_usuario_destino').select2({
			tags: false,
			tokenSeparators: [';', ' ']
		});

	});
</script>