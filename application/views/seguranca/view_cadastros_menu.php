<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          Criar model / controller
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
       	
       	<?php echo form_open('main/criar_mc'); ?> 

    	 <div class="row">
	      	<div class="col-md-5">
	      		<label for="link_model">Model (Arquivo)</label>
	      		<input name="link_model" id="link_model" class="form-control" placeholder="Model (Arquivo)" value="Model_">
	      	</div>
	      	<div class="col-md-5">
	      		<label for="descricao_model">Descrição</label>
	      		<input name="descricao_model" id="descricao_model" class="form-control" placeholder="Descrição">
	      	</div>
	     </div>

	      <div class="row">
	      	<div class="col-md-5">
	      		<label for="link_controller">Controler (Arquivo)</label>
	      		<input name="link_controller" id="link_controller" class="form-control" placeholder="Controler (Arquivo)" value="Controller_">
	      	</div>
	      	<div class="col-md-5">
	      		<label for="descricao_controller">Descrição</label>
	      		<input name="descricao_controller" id="descricao_controller" class="form-control" placeholder="Descrição">
	      	</div>
	      </div>

	      <hr>

	      <div class="row">
	      	<div class="col-md-2">	
	      		<input type="submit" name="enviar_mc" id="enviar_mc" class="btn btn-success" value="Cadastrar">
	      	</div>
	      </div>

      <?php echo form_close(); ?>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Criar Menu / Sub menu
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <?php echo form_open('main/criar_menu'); ?> 

	      <div class="row">

			<div class="col-md-3">
				<label>SubMenu de:</label>
	      		<select name="menu_acima" class="form-control">
	      			<option value="">Já é o Menu</option>
					<?php foreach ($menus as $chave => $menu) {
						echo '<option value="'.$menu->id_menu.'"> > '.$menu->descricao_menu.'</option>';
					} ?>
				</select>
	      	</div>

	      	<div class="col-md-3">
	      		<label for="titulo_menu">Título Menu</label>
	      		<input name="titulo_menu" id="titulo_menu" class="form-control" placeholder="Título menu">
	      	</div>
	      	<div class="col-md-3">
	      		<label for="descricao_menu">Descrição</label>
	      		<input name="descricao_menu" id="descricao_menu" class="form-control" placeholder="Descrição">
	      	</div>
	      	<div class="col-md-3">
	      		<label for="posicao_menu">Posição (Somente se for o menu)</label>
	      		<input name="posicao_menu" id="descricao_menu" class="form-control validar_numeros" placeholder="Posição">
	      	</div>
	      </div>

			<hr>

	      <div class="row">
	      	<div class="col-md-2">	
	      		<input type="submit" name="enviar_menu" id="enviar_menu" class="btn btn-success" value="Cadastrar">
	      	</div>
	      </div>

	      <?php echo form_close(); ?>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Criar Aplicação
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">

		<?php echo form_open('main/criar_aplicacao'); ?> 

	      <div class="row">

			<div class="col-md-3">
				<label>Menu:</label>
	      		<select name="menu" class="form-control">
	      			<option value="">Sem Menu</option>
					<?php foreach ($menus as $chave => $menu) {
						echo '<option value="'.$menu->id_menu.'"> > '.$menu->descricao_menu.'</option>';
					} ?>
				</select>
	      	</div>

	      	<div class="col-md-3">
				<label>Controller</label>
	      		<select name="fk_controller" class="form-control">
					<?php foreach ($controllers as $chave => $controller) {
						echo '<option value="'.$controller->id_controller.'">'.$controller->descricao_controller.'</option>';
					} ?>
				</select>
	      	</div>

	      </div>

	      <div class="row">

	      	<div class="col-md-3">
	      		<label for="link_aplicacao">Link Aplicação</label>
	      		<input name="link_aplicacao" id="link_aplicacao" class="form-control" placeholder="Link Aplicação" value="view_">
	      	</div>
	      	<div class="col-md-3">
	      		<label for="titulo_aplicacao">Título Aplicação</label>
	      		<input name="titulo_aplicacao" id="titulo_aplicacao" class="form-control" placeholder="Título Aplicação">
	      	</div>
	      	<div class="col-md-3">
	      		<label for="descricao_aplicacao">Descrição</label>
	      		<input name="descricao_aplicacao" id="descricao_aplicacao" class="form-control" placeholder="Descrição">
	      	</div>
	      </div>

	      <hr>

	      <div class="row">
	      	<div class="col-md-2">	
	      		<input type="submit" name="enviar_menu" id="enviar_menu" class="btn btn-success" value="Cadastrar">
	      	</div>
	      </div>

	      <?php echo form_close(); ?>

      </div>
    </div>
  </div>
</div>