<!DOCTYPE html>
<html lang="pt-br">

  <title>Protótipo | Login</title>
  <link rel="shortcut icon" href="<?php echo base_url() ?>style/img/favicon.ico">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap-tour.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/login.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap-tour.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>

  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

  <script type="text/javascript">
    $(document).ready(function(){
      var tour = new Tour({
        template: "<div class='popover tour' align='right'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>« Anterior</button><span data-role='separator'>|</span><button class='btn btn-default' data-role='next'>Próximo »</button></div><button class='btn btn-default' data-role='end'>Encerrar</button></div>",
        steps: [
        {
          element: "#login",
          title: "Entrar no Sistema",
          content: "Digite seu usuário e senha para ter acesso a seu histórico de ordens de serviço e outros benefícios"
        },
        {
          element: "#cadastro",
          title: "Crie sua conta",
          content: "Crie sua conta gratuitamente, seu e-mail deve ser válido pois através dele nas ordens de serviço que iremos identificá-lo."
        }
      ]});
      tour.init();
      tour.start();
    });
  </script>

<body>
<div class="main">
    
 <div class="row" style="position: absolute; top: 50px; width: 100%">
 <div class="col-md-4"></div>
 <div class="col-md-4" align="center">
    <label style="color: white; text-align: left; font-size: 13px;">Busque sem precisar acessar</label>
    <div class="input-group">
      <input type="text" class="form-control" id="buscar" placeholder="Busca rápida ordem de serviço">
      <span class="input-group-btn">
        <button class="btn btn-info buscar_os" style="height: 38px;" data-toggle="modal" data-target="#modal_acompanhamento" type="button">Buscar</button>
      </span>
    </div><!-- /input-group -->
  </div>
</div><!-- /.row -->   
    
    <div class="container">
<center>
<div class="middle">
      <div id="login">

        <form method="post" action="<?php echo base_url(); ?>main/login">

          <fieldset class="clearfix">

            <p ><span class="fa fa-user"></span><input type="text" id="usuario" name="usuario"  Placeholder="Usuário" required></p>
            <p><span class="fa fa-lock"></span><input type="password" name="senha"  Placeholder="Senha" required></p>
            
             <div>
                <span style="width:48%; font-size: 12px; text-align:left;  display: inline-block;"><a class="small-text" id="esqueciSenha" href="#">Esqueceu sua senha?</a></span>
                <span style="width:50%; text-align:right;  display: inline-block;"><input type="submit" class="btn btn-default acessar" style="height: 35px; width: 90%;" value="Entrar"></span>
            </div>

          </fieldset>
        </form>

      </div> <!-- end login -->
      <div class="logo">Protótipo
          <button class="btn btn-success" id="cadastro" data-toggle="modal" data-target="#modal_cadastro" style="width: 100%;">Crie sua conta</button>
          <div class="clearfix"></div>
      </div>
      
      </div>

      <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4" align="center">
            <div class="progress" id="img_load" style="margin-top: 20px;" hidden>
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                <span class="sr-only">Buscando...</span>
              </div>
            </div>
          </div>
        </div>

</center>
    </div>

</div>

<div class="row rodape">
    <div class="col-md-3" align="left">
      
    </div>
    <div class="col-md-6" align="center">
      <a href="promo">Protótipo</a>
    </div>
    <div class="col-md-3" align="right">
      Versão 3.3 Release
    </div>
</div>

<!-- Modal Cadastro -->
<div class="modal fade" id="modal_cadastro" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Criar nova conta</h4>
      </div>
      <div class="modal-body">
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <input class="form-control" name="cad_name" id="cad_name" placeholder="Nome">
        </div>
      </div>
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <input class="form-control mascara_cel" name="cad_tel" id="cad_tel" placeholder="Telefone">
        </div>
      </div>
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <input class="form-control validar_email" name="cad_email" id="cad_email" placeholder="E-mail">
        </div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <input class="form-control" name="cad_login" id="cad_login" placeholder="Login">
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Cadastrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /Modal Cadastro -->

<script type="text/javascript">
  $(document).ready(function(){

    $('.acessar').click(function(){
        $('#login').hide("slow");
        $('#img_load').show("slow");
    });

    $(".mascara_cel").mask('(99) 99999-9999', {'translation': {
            A: {pattern: /[A-Za-z0-9]/}
          }
    },{reverse: true}); 

    var erro_email = false;

    $(".validar_email").focusout(function(){

      console.log('Validando E-mail...');

      if($(this).val() != "") { 

         var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

         if(!filtro.test($(this).val())) {
    
          toast('E-mail', 'E-mail inválido!','error',true);
          erro_email = true;
          $(this).focus();
          return false;

         } else {

          erro_email = false;
          $.toast().reset('all');

         }

      } else {

        erro_email = false;
        $.toast().reset('all');

      }

   });

    if (!window.Notification) {
          console.log('Habilite as Notificações');
      } else {
          Notification.requestPermission().then(function(p){
              if (p === 'denied') {
                console.log('Notificações Não Aceitas.');
              } else if (p === 'granted') {
                console.log('Notificações Aceitas.');
              }
          });
      }

    $('#esqueciSenha').click(function(){

      if ($('#usuario').val() != "") {

        $('#login').hide();
        $('#img_load').show();

        $.ajax({
              url: "<?php echo base_url(); ?>controller_usuarios/esqueci_Senha",
              type: "post",
              data: {login: $("#usuario").val()},
              datatype: 'json',
              success: function(data){

                if (data['status'] == 1) {
                  toast('Senha alterada com sucesso!',data['resultado'],'success',false);
                } else {
                  toast('Senha não alterada ou erro ao enviar E-mail!',data['resultado'],'error',true);
                }

                 
                $('#login').show();
            $('#img_load').hide();
               

              },
              error:function(){
                toast('Falha','Falha ao alterar senha.','error',true);

                $('#login').show();
                $('#img_load').hide();
                  
              }   
            });

      } else {
        toast('Digite o usuário.','Usuário em branco.','error',true);
      }

    });

    <?php 
      //Caso a tentativa de login falhe
      if(isset($falha)){

        echo "$.toast({
                heading: 'Falha no login',
                text: [
                    'Usuário e/ou senha inválidos', 
                    'Ou Login e/ou Grupo inativo',
                    'Entre em contato com um Administrador.'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'error'
            });";
      }

      //Saiu do sistema.
      if(isset($saiu)){
        //Aviso quando outro login foi realizado
        $forcado_ = "";
        if (isset($forcado) && $forcado != "") {
          $forcado_ = "Novo acesso registrado em: ".$forcado;
        }

        echo "toast('Até logo ".$saiu."!','Acesso encerrado. ".$forcado_."','success',false);";

      }

    ?>

    $('.buscar_os').click(function(){

      $('#iframe_src').attr('src','<?php echo base_url() ?>controller_os/webviewAcompanhamentos?os='+$('#buscar').val());

    });

     $('.buscar_os2').click(function(){

      $('#iframe_src').attr('src','<?php echo base_url() ?>controller_os/webviewAcompanhamentos?os='+$('#buscar2').val());

    });

    function toast(titulo,mensagem,tipo,fixo){

      $.toast().reset('all');

      if (fixo) {

         $.toast({
            heading: titulo,
            text: mensagem,
            hideAfter: false,
            position: 'top-right',
            icon: tipo
        });

      } else {

        $.toast({
            heading: titulo,
            text: mensagem,
            showHideTransition: 'fade',
            position: 'top-right',
            hideAfter : 5000,  
            icon: tipo
        });

      } 
    }

  });
  </script>